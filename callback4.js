function cb4(cb1, cb2, cb3, board, lists, cards) {

    setTimeout(() => {
        board.find((x) => {
            if (x['name'] == 'Thanos') {
                cb1(x['id'], board, (err, data) => {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log(data);
                        cb2(data.id, lists, (err,data) => {
                            if (err) {
                                console.error(err)
                            } else {
                                console.log(data)
                                const obj_id = data.find((obj) => obj.name == 'Mind')
                                cb3(obj_id.id, cards, (err, data) => {
                                    if (err) {
                                        console.error(err)
                                    } else {
                                        console.log(data)
                                    }
                                })
                            }
                        })
                    }

                });
            }

        })


    }, 2 * 1000)
}

module.exports = cb4;