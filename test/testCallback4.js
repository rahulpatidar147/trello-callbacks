const cb1 = require('../callback1')
const cb2 = require('../callback2')
const cb3 = require('../callback3')
const cb4 = require('../callback4')
const board = require('../boards.json')
const lists = require('../lists.json')
const cards = require('../cards.json')

cb4(cb1,cb2,cb3,board,lists,cards)