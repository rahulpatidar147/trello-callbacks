function cb5(cb1, cb2, cb3, board, lists, cards) {

    setTimeout(() => {
        board.find((x) => {
            if (x['name'] == 'Thanos') {
                cb1(x['id'], board, (err, data) => {
                    if (err) {
                        console.error(err);
                    } else {
                        console.log(data);
                        cb2(data.id, lists, (err, data) => {
                            if (err) {
                                console.error(err)
                            } else {
                                console.log(data)
                                const obj1 = data.filter((obj) => obj.name == 'Mind' || obj.name == 'Space')
                                obj1.forEach(element => {

                                    cb3(element.id, cards, (err, data) => {
                                        if (err) {
                                            console.error(err)
                                        } else {
                                            console.log(data)
                                        }
                                    })
                                });
                            }
                        })
                    }

                });
            }

        })


    }, 2 * 1000)
}

module.exports = cb5;